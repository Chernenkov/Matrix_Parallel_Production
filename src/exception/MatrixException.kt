package exception

public class MatrixException(var msg: String) : RuntimeException(msg)