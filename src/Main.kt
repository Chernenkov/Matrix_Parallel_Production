import matrix.Matrix
import multiplier.ParallelMatrixCalculator

fun main(args: Array<String>){
        val m1 = Matrix(1000,1000)
        val m2 = Matrix(1000,1000)
        //var result: Matrix
        m1.randomFill(2,70)
        m2.randomFill(2,70)
        println("First matrix: ${m1.getHeight()}x${m1.getWidth()}")
        println()
        println("Second matrix: ${m2.getHeight()}x${m2.getWidth()}")
        println()
        println("Multiplying by one main thread: ")
        var timeBegin = System.currentTimeMillis()
        //result =
        m1.product(m2)
        println("Estimated in ${System.currentTimeMillis() - timeBegin} ms")
        //
        println()
        println("Multiplying by 2 threads: ")
        var multiThreadMultiplier = ParallelMatrixCalculator(2) // 2 threads here
        timeBegin = System.currentTimeMillis()
        //result =
        multiThreadMultiplier.multythreadProduct(m1, m2)
        println("Estimated in ${System.currentTimeMillis() - timeBegin} ms")
        println()
//        println("Multiplying by 4 threads:")
//        multiThreadMultiplier = ParallelMatrixCalculator(4)
//        timeBegin = System.currentTimeMillis()
//        result = multiThreadMultiplier.multythreadProduct(m1, m2)
//        println("Estimated in ${System.currentTimeMillis() - timeBegin} ms")
//        println()
        println("Multiplying by 8 threads:")
        multiThreadMultiplier = ParallelMatrixCalculator(8)
        timeBegin = System.currentTimeMillis()
        //result =
        multiThreadMultiplier.multythreadProduct(m1, m2)
        println("Estimated in ${System.currentTimeMillis() - timeBegin} ms")
        println()
        println("Multiplying by 16 threads:")
        multiThreadMultiplier = ParallelMatrixCalculator(16)
        timeBegin = System.currentTimeMillis()
        //result =
        multiThreadMultiplier.multythreadProduct(m1, m2)
        println("Estimated in ${System.currentTimeMillis() - timeBegin} ms")
        println()
        println("Multiplying by 47 threads:")
        multiThreadMultiplier = ParallelMatrixCalculator(47)
        timeBegin = System.currentTimeMillis()
        //result =
        multiThreadMultiplier.multythreadProduct(m1, m2)
        println("Estimated in ${System.currentTimeMillis() - timeBegin} ms")
//        println(m1.toString())
//        println(m2.toString())
//        var res: Matrix = m2.product(m1)
//        print(res.toString())
//        val matrix: Array<Array<Int>> = Array(3, {Array(3, {2})})
//        matrix[0][0] = 4
//        print(" " + matrix[0][0] + " " + matrix[0][1])
    }
