package matrix

import exception.MatrixException
import java.util.*


class Matrix (private val height:Int = 0,
              private val width: Int = 0) {
    private val matrix: Array<Array<Int>> = Array(height, {Array(width, {0})})
    //
    fun getSize(): Int {
        return (height * width)
    }
    //
    fun getHeight():Int = height
    fun getWidth(): Int = width
    //
    private fun setElem(i: Int,  j: Int, value: Int){
        if(i < 0 || j < 0 || i > getHeight() || j > getWidth()){
            throw MatrixException("Index out of bounds: [$i][$j]")
        }
        this.matrix[i][j] = value
    }
    //
    fun getElem(i: Int, j:Int) : Int{
        if(i < 0 || j < 0 || i > getHeight() || j > getWidth()){
            throw MatrixException("Index out of bounds: [$i][$j]")
        }
        return this.matrix[i][j]
    }
    //
    fun setElem(index: Int, value: Int){ // for multythread product
        setElem((index / getWidth()),(index % getWidth()),value)
    }
    //
    fun product(operand: Matrix) : Matrix {
        if(getWidth() != operand.getHeight()){
            throw MatrixException("Wrong dimensions: height of M1 should be equal to width of M2...")
        }
        val result = Matrix(getHeight(), operand.getWidth())
        //
        var tmp: Int
        for (i in 0 until result.getHeight() - 1){
            for (j in 0 until result.getWidth() - 1){
                tmp = 0
                for(h in 0 until this.getWidth() - 1){
                    tmp += this.getElem(i, h) * operand.getElem(h, j)
                }
                result.setElem(i, j, tmp)
            }
        }
        //
        return result
    }
    //
    fun randomFill(border1: Int, border2: Int){
        val rand = Random()
        for (i in 0 until  getHeight() - 1){
            for (j in 0 until  getWidth() - 1){
                setElem(i, j, rand.nextInt(Math.abs(border2 - border1)))
            }
        }
    }
    //
    override fun toString(): String {
        var result = ""
        for (i in 0 until  getHeight() - 1){
            result += "\n"
            for (j in 0 until  getWidth() - 1){
                result += "" + matrix[i][j] + " "
            }
        }
        return result
    }
    //
    override fun equals(other: Any?): Boolean {
        if(other !is Matrix) return false
        if(this == other) return true
        val tmpObj: Matrix = other //as Matrix
        if(getHeight() != tmpObj.getHeight() || getWidth() != tmpObj.getWidth()) return false
        for(i in 0 until getHeight() - 1){
            for (j in 0 until getWidth() - 1){
                if(tmpObj.getElem(i,j) != this.getElem(i, j)) return false
            }
        }
        return true
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
