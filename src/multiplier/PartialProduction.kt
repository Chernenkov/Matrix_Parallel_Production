package multiplier

import matrix.Matrix

class PartialProduction(private var start: Int,
                        end: Int,
                        private var M1: Matrix,
                        private var M2: Matrix,
                        private var result:Matrix) : Thread() {
    private var finish = start + end
    override fun run(){
        var i: Int
        var j: Int
        var temp: Int
        println("Thread starts...")
        var indx: Int = start
        while(indx < finish && indx < result.getSize()){
            temp = 0
            i = indx / result.getWidth()
            j = indx / result.getHeight()
            for (h in 0 until M1.getWidth() - 1){
                temp += M1.getElem(i, h) * M2.getElem(h, j)
            }
            result.setElem(indx, temp)
            indx++
        }
        println("Thread ends...")
    }
}