package multiplier

import exception.MatrixException
import matrix.Matrix

class ParallelMatrixCalculator(private val threadNumber: Int) {
    private var threads = ArrayList<Thread>(threadNumber)
    //
    @Throws (InterruptedException::class)
    fun multythreadProduct(m1: Matrix, m2: Matrix) : Matrix {
        if(m1.getHeight() != m2.getWidth()) throw MatrixException("Wrong dimensions: height of M1 should be equal to width of M2...")
        val result = Matrix(m1.getHeight(), m2.getWidth())
        val cellsOnThread: Int = (result.getSize() / threadNumber)
        var i = 0
        while(i < result.getSize()){
            threads.add(Thread(PartialProduction(i, cellsOnThread, m1, m2, result)))
            i += cellsOnThread
        }
        for(t: Thread in threads) {
            t.start()
            Thread.sleep(2) // test this number or just split join and start to different cycles
            if(t.isAlive) t.join()
        }
        return result
    }
}